# frozen_string_literal: true

control 'zfs-package-install-pkg-installed' do
  title 'it should be installed'

  describe package('zfsutils-linux') do
    it { should be_installed }
  end
end
