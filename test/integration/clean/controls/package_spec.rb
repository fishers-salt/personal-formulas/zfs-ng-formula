# frozen_string_literal: true

control 'zfs-package-clean-pkg-removed' do
  title 'it should not be installed'

  describe package('zfsutils-linux') do
    it { should_not be_installed }
  end
end
