# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as zfs_ng with context %}

zfs-package-install-pkg-installed:
  pkg.installed:
    - name: {{ zfs_ng.pkg.name }}
